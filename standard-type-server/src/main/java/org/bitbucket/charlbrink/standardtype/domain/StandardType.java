package org.bitbucket.charlbrink.standardtype.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
public class StandardType {
    @JsonIgnore
    @Id
    @GeneratedValue
    private Long id;

    private String identity;
    private String name;
    private String description;

    public StandardType() {
    }

    public StandardType(String identity, String name, String description) {
        this.identity = identity;
        this.name = name;
        this.description = description;
    }

}
