package org.bitbucket.charlbrink.standardtype.repository;

import org.bitbucket.charlbrink.standardtype.domain.StandardType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StandardTypeRepository extends JpaRepository<StandardType, Long> {
    public Page<StandardType> findByIdentity(String identity, Pageable pageable);
    public StandardType findByIdentityAndName(String identity, String name);
    public StandardType deleteByIdentityAndName(String identity, String name);
    public List<StandardType> deleteAllByIdentity(String identity);
}
