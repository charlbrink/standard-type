package org.bitbucket.charlbrink.standardtype.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.bitbucket.charlbrink.standardtype.domain.IdentityNotFoundException;
import org.bitbucket.charlbrink.standardtype.domain.StandardType;
import org.bitbucket.charlbrink.standardtype.domain.StandardTypeNotFoundException;
import org.bitbucket.charlbrink.standardtype.repository.StandardTypeRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(value="standardTypes", description="Operations pertaining to standard types")
@RequestMapping("/types")
@RestController
public class StandardTypeController {
    private StandardTypeRepository standardTypeRepository;

    public StandardTypeController(StandardTypeRepository standardTypeRepository) {
        this.standardTypeRepository = standardTypeRepository;
    }

    @ApiOperation(value = "Retrieve a standard type", response = StandardType.class)
    @GetMapping("/{identity}/{name}")
    public @ResponseBody StandardType get(@PathVariable String identity, @PathVariable String name) {
        StandardType standardType = standardTypeRepository.findByIdentityAndName(identity, name);
        if (standardType == null) {
            throw new StandardTypeNotFoundException(String.format("StandardType does not exist for identity %s and name %s", identity, name));
        }
        return standardType;
    }

    @ApiOperation(value = "Retrieve a page of standard types for a given identity", response = PageImpl.class)
    @GetMapping("/{identity}")
    public @ResponseBody Page<StandardType> getAll(@PathVariable String identity, Pageable pageable) {
        Page<StandardType> page = standardTypeRepository.findByIdentity(identity, pageable);
        if (page == null) {
            throw new IdentityNotFoundException(String.format("Identity %s does not exist", identity));
        }
        return page;
    }

    @ApiOperation(value = "Add a standard type", response = StandardType.class)
    @PostMapping()
    public @ResponseBody StandardType create(@RequestBody StandardType standardType) {
        return standardTypeRepository.save(standardType);
    }

    @ApiOperation(value = "Update a standard type", response = StandardType.class)
    @PutMapping
    public @ResponseBody StandardType update(@RequestBody StandardType standardType) {
        return standardTypeRepository.save(standardType);
    }

    @ApiOperation(value = "Delete a standard type", response = StandardType.class)
    @DeleteMapping("/{identity}/{name}")
    public @ResponseBody StandardType delete(@PathVariable String identity, @PathVariable String name) {
        return standardTypeRepository.deleteByIdentityAndName(identity, name);
    }

    @ApiOperation(value = "Delete all standard types for a given identity", response = List.class)
    @DeleteMapping("/{identity}")
    public @ResponseBody List<StandardType> deleteAll(@PathVariable String identity) {
        return standardTypeRepository.deleteAllByIdentity(identity);
    }

}
