package org.bitbucket.charlbrink.standardtype.domain;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class StandardTypeNotFoundException extends RuntimeException {
    public StandardTypeNotFoundException() {
    }

    public StandardTypeNotFoundException(String message) {
        super(message);
    }

    public StandardTypeNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public StandardTypeNotFoundException(Throwable cause) {
        super(cause);
    }

    public StandardTypeNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
