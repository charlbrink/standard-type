package org.bitbucket.charlbrink.standardtype.domain;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class IdentityNotFoundException extends RuntimeException {
    public IdentityNotFoundException() {
    }

    public IdentityNotFoundException(String message) {
        super(message);
    }

    public IdentityNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public IdentityNotFoundException(Throwable cause) {
        super(cause);
    }

    public IdentityNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
