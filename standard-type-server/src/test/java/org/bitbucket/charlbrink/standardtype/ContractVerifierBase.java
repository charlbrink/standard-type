package org.bitbucket.charlbrink.standardtype;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.bitbucket.charlbrink.standardtype.domain.StandardType;
import org.bitbucket.charlbrink.standardtype.repository.StandardTypeRepository;
import org.bitbucket.charlbrink.standardtype.web.StandardTypeController;
import org.junit.Before;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@EnableSpringDataWebSupport
public class ContractVerifierBase {

    @Before
    public void setup() {

        RestAssuredMockMvc.mockMvc(MockMvcBuilders
                .standaloneSetup(new StandardTypeController(new StubStandardTypeRepository()))
                .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver()).build());
    }

    private class StubStandardTypeRepository implements StandardTypeRepository {

        @Override
        public Page<StandardType> findByIdentity(String identity, Pageable pageable) {
            List<StandardType> standardTypes = new ArrayList<>();
            for (int i = 0 ; i < 3; i++) {
                standardTypes.add(new StandardType(identity, identity + "" + i, identity + "" + i + " description"));
            }
            Page<StandardType> result = new PageImpl<>(standardTypes, Pageable.unpaged(), 3);
            return result;
        }

        @Override
        public StandardType findByIdentityAndName(String identity, String name) {
            StandardType standardType = new StandardType(identity, name, name+ " description");
            return standardType;
        }

        @Override
        public StandardType deleteByIdentityAndName(String identity, String name) {
            return null;
        }

        @Override
        public List<StandardType> deleteAllByIdentity(String identity) {
            return null;
        }

        @Override
        public <S extends StandardType> S save(S s) {
            StandardType standardTypeToSave = s;
            standardTypeToSave.setId(1L);
            return s;
        }

        @Override
        public Optional<StandardType> findById(Long aLong) {
            return Optional.empty();
        }

        @Override
        public boolean existsById(Long aLong) {
            return false;
        }

        @Override
        public List<StandardType> findAll() {
            return null;
        }

        @Override
        public List<StandardType> findAll(Sort sort) {
            return null;
        }

        @Override
        public Page<StandardType> findAll(Pageable pageable) {
            return null;
        }

        @Override
        public List<StandardType> findAllById(Iterable<Long> iterable) {
            return null;
        }

        @Override
        public long count() {
            return 0;
        }

        @Override
        public void deleteById(Long aLong) {

        }

        @Override
        public void delete(StandardType standardType) {

        }

        @Override
        public void deleteAll(Iterable<? extends StandardType> iterable) {

        }

        @Override
        public void deleteAll() {

        }

        @Override
        public void flush() {

        }

        @Override
        public void deleteInBatch(Iterable<StandardType> iterable) {

        }

        @Override
        public void deleteAllInBatch() {

        }

        @Override
        public StandardType getOne(Long aLong) {
            return null;
        }

        @Override
        public <S extends StandardType> List<S> findAll(Example<S> example, Sort sort) {
            return null;
        }

        @Override
        public <StandardType extends org.bitbucket.charlbrink.standardtype.domain.StandardType> List<StandardType> findAll(Example<StandardType> example) {
            return null;
        }

        @Override
        public <S extends StandardType> S saveAndFlush(S s) {
            return null;
        }

        @Override
        public <S extends StandardType> List<S> saveAll(Iterable<S> iterable) {
            return null;
        }

        @Override
        public <S extends StandardType> Optional<S> findOne(Example<S> example) {
            return Optional.empty();
        }

        @Override
        public <S extends StandardType> Page<S> findAll(Example<S> example, Pageable pageable) {
            return null;
        }

        @Override
        public <S extends StandardType> long count(Example<S> example) {
            return 0;
        }

        @Override
        public <S extends StandardType> boolean exists(Example<S> example) {
            return false;
        }
    }

}
