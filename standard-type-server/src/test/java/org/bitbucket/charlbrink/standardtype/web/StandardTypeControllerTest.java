package org.bitbucket.charlbrink.standardtype.web;

import org.bitbucket.charlbrink.standardtype.Application;
import org.bitbucket.charlbrink.standardtype.domain.StandardType;
import org.bitbucket.charlbrink.standardtype.repository.StandardTypeRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.Charset;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class StandardTypeControllerTest {
    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private StandardTypeRepository standardTypeRepository;

    private MockMvc mockMvc;

    private StandardType standardType;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
        this.standardTypeRepository.deleteAllInBatch();

        this.standardType = standardTypeRepository.save(new StandardType("address", "residential", "Residential"));
        this.standardType = standardTypeRepository.save(new StandardType("address", "postal", "Postal"));
        this.standardType = standardTypeRepository.save(new StandardType("address", "work", "Work"));
    }

    @Test
    public void givenValidIdentity_whenGet_thenExpectOneResult() throws Exception {
        mockMvc.perform(get("/types/address/residential/")
                .contentType(contentType))
                .andExpect(status().isOk());
    }

    @Test
    public void givenInvalidIdentity_whenGetAll_thenThrowException() throws Exception {
        mockMvc.perform(get("/types/address/business/")
                .contentType(contentType))
                .andExpect(status().isNotFound());
    }

}