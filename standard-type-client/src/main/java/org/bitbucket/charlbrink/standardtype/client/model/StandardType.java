package org.bitbucket.charlbrink.standardtype.client.model;

import lombok.Data;

@Data
public class StandardType {
    private String identity;
    private String name;
    private String description;

    public StandardType() {
    }

    public StandardType(String identity, String name, String description) {
        this.identity = identity;
        this.name = name;
        this.description = description;
    }

}
