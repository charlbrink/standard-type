package org.bitbucket.charlbrink.standardtype.client;

import org.bitbucket.charlbrink.standardtype.client.model.StandardType;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@FeignClient(name="${feign.standardtype.name}", url="${feign.standardtype.url}")
public interface StandardTypeResource {

    @RequestMapping(value = "/types/{identity}/{name}", method = GET, consumes = "application/json", produces = "application/json")
    ResponseEntity<StandardType> get(@PathVariable("identity") String identity, @PathVariable("name") String name);

}
