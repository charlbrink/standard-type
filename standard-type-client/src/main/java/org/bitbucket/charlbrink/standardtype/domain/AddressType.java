package org.bitbucket.charlbrink.standardtype.domain;

/**
 * Example of how a standard type can be implemented using enum
 */
public enum AddressType {
    RESIDENTIAL {
        @Override
        public boolean isResidential() {
            return true;
        }
    },
    POSTAL {
        @Override
        public boolean isPostal() {
            return true;
        }
    },
    WORK {
        @Override
        public boolean isWork() {
            return true;
        }
    };

    public boolean isResidential() {
        return false;
    }

    public boolean isPostal() {
        return false;
    }

    public boolean isWork() {
        return false;
    }

}
