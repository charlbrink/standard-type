package org.bitbucket.charlbrink.standardtype;

import lombok.extern.slf4j.Slf4j;
import org.bitbucket.charlbrink.standardtype.client.model.StandardType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@Slf4j
@EnableFeignClients(basePackages = {"org.bitbucket.charlbrink.standardtype"})
@SpringBootApplication
public class Application implements CommandLineRunner {

    @Autowired
    private MyService myService;

    public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

    @Override
    public void run(String... args) throws Exception {
        StandardType standardType = myService.retrieveCountry("ZA");
        log.info("Returned standardType", standardType);
    }

}
