package org.bitbucket.charlbrink.standardtype;

import lombok.extern.slf4j.Slf4j;
import org.bitbucket.charlbrink.standardtype.client.StandardTypeResource;
import org.bitbucket.charlbrink.standardtype.client.model.StandardType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class MyService {
    private StandardTypeResource standardTypeResource;;

    public MyService(final StandardTypeResource standardTypeResource) {
        this.standardTypeResource = standardTypeResource;
    }

    public StandardType retrieveCountry(final String code) {
        ResponseEntity<StandardType> response = standardTypeResource.get("country", code);
        log.info("Received http status code {}", response.getStatusCode());
        if (response.hasBody()) {
            log.info("Received response with starndardType {}", response.getBody());
        }
        return response.getBody();
    }
}
