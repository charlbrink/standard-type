package org.bitbucket.charlbrink.standardtype;

import org.bitbucket.charlbrink.standardtype.client.StandardTypeResource;
import org.bitbucket.charlbrink.standardtype.client.model.StandardType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureStubRunner(stubsMode = StubRunnerProperties.StubsMode.LOCAL)
public class StandardTypeClientTest {

    @Autowired
    StandardTypeResource standardTypeResource;

    @Test
    public void givenRequestForCountryZA_whenGet_thenExpectSuccess() {
        ResponseEntity<StandardType> response = standardTypeResource.get("country", "ZA");
        assertThat("Expected success", response.getStatusCode(), is(HttpStatus.OK));
        assertThat("Expected success", response.getBody().getIdentity(), is("country"));
        assertThat("Expected success", response.getBody().getName(), is("ZA"));
        assertThat("Expected success", response.getBody().getDescription(), is("ZA description"));
    }
}
